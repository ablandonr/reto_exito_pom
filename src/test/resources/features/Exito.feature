#Author: ablandonr@choucairtesting.com

@tag
Feature: Navegación por la página del Éxito
  El objetivo es ingresar a la página del éxito y ver 
  los televisores disponibles.

  @CasoExitoso
  Scenario: Televisiores en la tienda virtual del éxito
    Given que Aleja quiere comprar un televisor para ver el mundial de futbol
    When ingreso a la página virtual del éxito y selecciono el televisor que más me gusta 
    Then realizo la compra y en el texto de añadir carrito debe aparecer 1 productos en tu carrito.
