package com.choucair.formacion.pageobjects;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class RealizarBusquedaTV extends PageObject{
	//Barra de búsqueda
	@FindBy (id="tbxSearch")
	public WebElementFacade busqueda;
	
	//Botón de búsqueda
	@FindBy (id="btnSearch")
	public WebElementFacade botonBusqueda;
	
	//Botón de pulgadas
	@FindBy (xpath="//*[@id=\'filterBy\']/div[2]/div[2]/ul/li[2]/a")
	public WebElementFacade campoPulgadas;
	
	//Botón de marca
	@FindBy (xpath="//*[@id=\'filterBy\']/div[2]/div[2]/ul/li[1]/label/input")
	public WebElementFacade campoMarca;
	
	//Botón de definición de pantalla
	@FindBy (xpath="//*[@id=\'filterBy\']/div[3]/div[2]/ul/li[3]/a")
	public WebElementFacade campoDefinicion;
	
	//Mensaje emergente
	@FindBy (xpath="//*[@id='close-wisepop-123971']/img")
	public WebElementFacade cerrarEmergente;
	
	//Click en la pantalla
	@FindBy (xpath="//*[@id=\'header\']")
	public WebElementFacade toda;
	
	//TV Seleccionado
	@FindBy (xpath="//*[@id=\'prd0002747648749062\']/div/div/a[1]/div[1]/h1")
	public WebElementFacade seleccionTV;
	
	//Lista de televisores
	@FindBy (xpath="//*[@id=\'plp\']/div[4]/div[2]/div[2]")
	public WebElementFacade listaTV;

	//Añadir al carrito
	@FindBy (xpath="//*[@id=\'prd0002747648749062\']/div[3]/div[2]/div[2]/button")
	public WebElementFacade btnAnadir;
	
	//Dar click en el carrito
	@FindBy (xpath="//*[@id=\'headerUserInfo\']/div[2]/a/span[2]")
	public static WebElementFacade btnCarri;
	
	public void Buscando(String palabra) {
		busqueda.sendKeys(palabra);
		botonBusqueda.click();
	}
	public void EspecificacionesPul() {
		campoPulgadas.click();
	}
	public void EspecificacionesMar() {
		campoMarca.click();
	}
	public void EspecificacionesDef() {
		campoDefinicion.click();
	}
	public void Emergente() {
		cerrarEmergente.click();
	}
	public void Pantalla() {
		toda.click();
	}
	public void SeleccionarTV() {
		seleccionTV.click();
	}
	public void BuscarEnLista() {
		List<WebElement> lista_TV = listaTV.findElements(By.tagName("a"));
			for(int i=0; i<lista_TV.size(); i++ )
	        {
	        	System.out.println(lista_TV.get(i).getText());
	        }
	}
	public void Carrito() {
		btnAnadir.click();
	}
	public void VerCarrito() {
		btnCarri.click();
	}

}
