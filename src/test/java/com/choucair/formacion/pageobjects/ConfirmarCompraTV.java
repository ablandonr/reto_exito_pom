package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class ConfirmarCompraTV extends PageObject{
	
	@FindBy (xpath="//*[@id=\'summary-page\']/div[1]/h4")
	public static WebElementFacade txtCarrito;
	
	public void TextoCarrito(String texto) {
		assertThat(txtCarrito.containsText(texto), is(true));
	}

}
