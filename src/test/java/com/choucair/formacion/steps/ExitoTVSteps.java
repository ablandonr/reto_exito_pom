package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.IngresarAExito;

import net.thucydides.core.annotations.Step;

public class ExitoTVSteps {
	
	IngresarAExito ingresarAExito;
	@Step
	public void exito_TV() throws InterruptedException {
		ingresarAExito.open();
		Thread.sleep(10000);
	}

}
