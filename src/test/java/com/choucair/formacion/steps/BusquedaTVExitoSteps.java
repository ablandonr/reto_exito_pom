package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.RealizarBusquedaTV;
import net.thucydides.core.annotations.Step;

public class BusquedaTVExitoSteps {
	
	RealizarBusquedaTV realizarBusquedaTV;
	@Step
	public void buscaTV(String clave) throws InterruptedException {
		Thread.sleep(10000);
		realizarBusquedaTV.Buscando(clave);
		realizarBusquedaTV.EspecificacionesPul();
		realizarBusquedaTV.Emergente();
		Thread.sleep(10000);
		realizarBusquedaTV.EspecificacionesDef();
		realizarBusquedaTV.EspecificacionesMar();
		realizarBusquedaTV.BuscarEnLista();
		realizarBusquedaTV.SeleccionarTV();	
		realizarBusquedaTV.Carrito();
		realizarBusquedaTV.VerCarrito();
	}
	

	
}
