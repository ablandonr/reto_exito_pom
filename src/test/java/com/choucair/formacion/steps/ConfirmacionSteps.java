package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.ConfirmarCompraTV;

import net.thucydides.core.annotations.Step;

public class ConfirmacionSteps {
	ConfirmarCompraTV confirmarCompraTV;
	@Step 
	public void Carrito(String txtConfirmar) {
		confirmarCompraTV.TextoCarrito(txtConfirmar);
	}
}
