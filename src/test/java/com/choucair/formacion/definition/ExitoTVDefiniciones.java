package com.choucair.formacion.definition;

import com.choucair.formacion.steps.BusquedaTVExitoSteps;
import com.choucair.formacion.steps.ConfirmacionSteps;
import com.choucair.formacion.steps.ExitoTVSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ExitoTVDefiniciones {
	
	@Steps
	ExitoTVSteps exitoTVSteps;
	@Steps
	BusquedaTVExitoSteps busquedaTVExitoSteps;
	@Steps
	ConfirmacionSteps confirmacionSteps;
	
	@Given("^que Aleja quiere comprar un televisor para ver el mundial de futbol$")
	public void que_Aleja_quiere_comprar_un_televisor_para_ver_el_mundial_de_futbol() throws Throwable {
		exitoTVSteps.exito_TV();
	}

	@When("^ingreso a la página virtual del éxito y selecciono el (.*) que más me gusta$")
	public void ingreso_a_la_página_virtual_del_éxito_y_selecciono_el_que_más_me_gusta(String clave) throws Throwable {
		busquedaTVExitoSteps.buscaTV(clave);
	}

	@Then("^realizo la compra y en el texto de añadir carrito debe aparecer (.*)\\.$")
	public void realizo_la_compra_y_en_el_texto_de_añadir_carrito_debe_aparecer_productos_en_tu_carrito(String txtConfirma) throws Throwable {
		confirmacionSteps.Carrito(txtConfirma);
	}


}
